# Lain.la Shortener Gui

This is a gui wrapper for the link shortener provided by [https://lain.la](https://lain.la)

A CLI wrapper is also included.

## To Build:
- Open Solution in [Visual Studio 2022](https://visualstudio.microsoft.com/)
- Right click LainaLa.WPF and hit publish
    - For CLI version right click LainLa instead
- Look over the publishing settings to confirm it is correct for you computer
- Hit Publish
