﻿namespace LainLa.Models
{
    public class UrlShortener
    {
        HttpClient httpClient;
        
        public UrlShortener()
        {
            httpClient = new HttpClient();
        }

        public async Task<string> ShortenUrl(string url)
        {
            // Create the HttpContent for the form to be posted
            var requestContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("url", url)
            });

            // Get the Response
            HttpResponseMessage response = await httpClient.PostAsync(
                "https://s.lain.la",
                requestContent);

            // Get the Stream of Content
            using (var reader = new StreamReader(await response.Content.ReadAsStreamAsync()))
            {
                // Write the output
                return await reader.ReadToEndAsync();
            }
        }

        public async Task<IEnumerable<string>> ShortenUrl(IEnumerable<string> urls)
        {
            List<string> results = new List<string>();

            foreach (var url in urls)
            {
                results.Add(await ShortenUrl(url));
            }

            return results;
        }
    }
}
