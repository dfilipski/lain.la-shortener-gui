﻿using LainLa.Models;

namespace LainLa
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // Validate Arguments
            if (args.Length == 0)
            {
                Console.WriteLine("Please include at least one URL to shorten");
                return;
            }

            var shortener = new UrlShortener();

            var results = await shortener.ShortenUrl(args);

            foreach (var r in results)
            {
                Console.WriteLine(r);
            }
        }
    }
}