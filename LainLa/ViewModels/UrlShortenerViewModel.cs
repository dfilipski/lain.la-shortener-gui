﻿using LainLa.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LainLa.ViewModels
{
    public class UrlShortenerViewModel : ViewModelBase
    {
        UrlShortener shortener;
        private string shortUrl;

        public string Url { get; set; }
        public string ShortUrl 
        { 
            get => shortUrl; 
            set
            {
                shortUrl = value;
                RaisePropertyChanged();
            }
        }

        public DelegateCommand ShortenUrlCommand { get; set; }

        public UrlShortenerViewModel()
        {
            shortener = new UrlShortener();
            ShortenUrlCommand = new DelegateCommand(ShortenUrl);
        }

        public async void ShortenUrl()
        {
            ShortUrl = await shortener.ShortenUrl(Url);
        }

    }
}
